import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isLoading: {
      title: "Loading...",
      state: false,
      color: "primary",
    },
  },
  mutations: {
    showLoading(state, payload) {
      state.isLoading.title = payload.title;
      state.isLoading.color = payload.color;
      state.isLoading.state = true;
    },
    hideLoading(state) {
      state.isLoading.state = false;
    },
  },
  actions: {},
  modules: {},
});
